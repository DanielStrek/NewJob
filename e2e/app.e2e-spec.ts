import { NewJobPage } from './app.po';

describe('new-job App', () => {
  let page: NewJobPage;

  beforeEach(() => {
    page = new NewJobPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
